import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import jwt from 'jsonwebtoken';
import _ from 'lodash';
import { setStorage, getStorage } from '@/utils/loca';

export interface IUserInfo {
    email: string;
    type: string;
    source: string;
    user_id: string;
    role: string;
    role_v2: string;
    entity_id: string;
    entity: string;
    entity_type: string;
    entity_name: string;
    entity_image: string;
    permissions: string[];
    mfa_login_passed: boolean;
    iat: number;
    nbf: number;
    exp: number;
    jti: string;
}

export interface AuthModelState {
    token: string | null;
    userInfo: IUserInfo | null
    isLogin: boolean
}

export interface AuthModelType {
    namespace: 'auth';
    state: AuthModelState;
    reducers: {
        setToken: Reducer<AuthModelState>;
    };
}

const AuthModel: AuthModelType = {
    namespace: 'auth',
    state: {
        token: getStorage('token'),
        userInfo: getStorage('userInfo', true),
        isLogin: !!getStorage('token')
    },
    reducers: {
        setToken(state, action) {
            const token = action.payload
            const userInfo = jwt.decode(token) as IUserInfo
            debugger
            setStorage('token', token)
            setStorage('userInfo', userInfo)

            return { ...state, token: token, userInfo: userInfo, isLogin: true }
        }
    }
}

export default AuthModel