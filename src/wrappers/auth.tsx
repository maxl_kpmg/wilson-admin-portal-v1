import { FC } from 'react';
import { Redirect, connect, ConnectProps, AuthModelState } from 'umi';

interface AuthProps extends ConnectProps {
  auth: AuthModelState;
}

const Auth: FC<AuthProps> = (props) => {
  const { auth, children } = props;

  if (auth.isLogin) {
    return <div>{children}</div>;
  } else {
    return <Redirect to="/login" />;
  }
};

export default connect(({ auth }: { auth: AuthModelState }) => ({
  auth,
}))(Auth);
