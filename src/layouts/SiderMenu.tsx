import React, { FC } from 'react';
import { Layout, Menu } from 'antd';
import {
  history,
  Location,
  History,
  connect,
  ConnectProps,
  AuthModelState,
} from 'umi';
import { routes } from '@/routes';
import logo from '@/assets/img/logo-wil.png';

import _ from 'lodash';

const { Sider } = Layout;

interface SiderMenuProps extends ConnectProps {
  auth: AuthModelState;
}

const SiderMenu: FC<SiderMenuProps> = ({ location, auth }) => {
  return (
    <Sider theme="light">
      <div className="wil_logo">
        <img src={logo} alt="logo" />
        <span>
          {auth!
            .userInfo!.role.split('_')
            .map((x) => _.capitalize(x))
            .join(' ')}
        </span>
      </div>
      <Menu
        // style={{ height: '100%' }}
        defaultSelectedKeys={[`/${location.pathname.split('/')[1]}`]}
        mode="inline"
        onClick={(e) => history.push(e.key)}
      >
        {routes
          ?.filter((x) => x.hideInMenu)
          .map((x) => (
            <Menu.Item key={x.path} icon={x.icon && <x.icon />}>
              {x.name}
            </Menu.Item>
          ))}
      </Menu>
    </Sider>
  );
};

export default connect(({ auth }: { auth: AuthModelState }) => ({ auth }))(
  SiderMenu,
);
