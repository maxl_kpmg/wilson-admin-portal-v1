import { Breadcrumb } from 'antd';
import { FC, useEffect, useState } from 'react';
import { IRouteComponentProps, Link } from 'umi';
import _, { values } from 'lodash';

interface IBreadcrumbData {
  key: string;
  value: string;
  path: string;
}

const ContentHearder: FC<IRouteComponentProps> = ({ location, route }) => {
  const [breadcrumbData, setBreadcrumbData] = useState<IBreadcrumbData[]>([]);

  const handleData = (pathname: string) => {
    return pathname
      .split('/')
      .filter(Boolean)
      .reduce((x: any, y: any, i: number) => {
        const tmp = x[i - 1]?.key || '';
        const val = `${tmp}/${y}`;
        let result = {
          key: val,
          value: route.routes!.find((r) => r.path === val)!.name,
          path: route.routes!.find((r) => r.path === val)!.path,
        };
        return [...x, result];
      }, []);
  };

  useEffect(() => {
    const { pathname } = location;
    let array: IBreadcrumbData[] = [];
    if (pathname === '/') {
      array = [{ key: '/', value: 'dashboard', path: '/' }];
    }
    array = handleData(pathname);

    setBreadcrumbData(array);
  }, [location.pathname]);
  return (
    <div className="content_hearder">
      <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>
          <Link to="/">Home</Link>
        </Breadcrumb.Item>
        {breadcrumbData.map((item) => (
          <Breadcrumb.Item key={item.value}>
            <Link to={item.path}>{item.value}</Link>
          </Breadcrumb.Item>
        ))}
      </Breadcrumb>
    </div>
  );
};

export default ContentHearder;
