import { FC } from 'react';
import { Layout, Breadcrumb } from 'antd';
import { IRouteComponentProps } from 'umi';

import ContentHeader from './ContentHeader';
import SiderMenu from './SiderMenu';

import './layouts.less';

const { Header, Content, Footer } = Layout;

const BasicLayout: FC<IRouteComponentProps> = (props) => {
  return (
    <Layout className="wil_layouts">
      <SiderMenu {...props} />
      <Layout>
        <Header>Header</Header>
        <Content>
          <ContentHeader {...props} />
          <div className="wil_container">{props.children}</div>
        </Content>
        <Footer>
          <span>
            <a href="https://kpmg.com.sg/" target="_blank">
              UNIFY. &copy; 2020 KPMG DV Singapore
            </a>
          </span>
        </Footer>
      </Layout>
    </Layout>
  );
};

export default BasicLayout;
