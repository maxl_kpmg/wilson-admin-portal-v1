import request from './request';

export interface IAuth {
    email: string;
    password: string;
}

const login = (user: IAuth) => {
    return request.post('/api/admin/auth/login?scope=admin', user)
}

export {
    login
}