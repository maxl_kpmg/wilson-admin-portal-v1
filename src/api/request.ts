import axios from 'axios';
import _ from 'lodash';
import { history } from 'umi';
import { reomveStorage } from '@/utils/loca';
import { message } from 'antd';

const instance = axios.create({
    baseURL: '/dev'
})

instance.interceptors.request.use((config) => {
    const token = localStorage.getItem('token')
    if (token) {
        config.headers = {
            Authorization: 'Bearer' + ' ' + token,
            ...config.headers
        }
    }
    return config
})

instance.interceptors.response.use((data) => {
    return data
}, (error) => {
    const statusCode = _.get(error, 'response.data.statusCode', '')

    if (statusCode === 401) {
        history.push('/login')
        reomveStorage('userInfo')
        reomveStorage('token')
    } else if (_.get(error, 'response.status', '') === 503) {
        message.error(`网络错误`)
    }
})

export default instance