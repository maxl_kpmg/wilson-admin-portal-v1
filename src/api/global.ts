export interface IQuery {
    limit?: number;
    page?: number;
    key?: string;
    sort?: string;
    dir?: string;
    offset?: number;
}

export interface IResult<T> {
    total: number;
    list: T;
}