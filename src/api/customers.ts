import request from './request';
import qs from 'qs';
import { IQuery } from './global';
import { downloadFile } from '@/utils';

const getCustomerList = (query?: IQuery) => {
    return request.get(`/api/admin/data/customers?` + qs.stringify(query))
}

const exportCustomerCSV = async (query?: IQuery) => {
    const queryString = query ? `?${query}` : '';
    const result = await request.get(
        '/api/admin/data/customers/downloadCsv' + queryString,
        {
            headers: {
                'Content-Type': 'text/csv',
            }
        }
    );

    downloadFile(result.data, 'file.csv')
    return 'ok'
}

const getCustomerById = (id: string) => {
    return request.get(`/api/admin/data/customers/${id}`)
}

export {
    getCustomerList,
    exportCustomerCSV,
    getCustomerById
}