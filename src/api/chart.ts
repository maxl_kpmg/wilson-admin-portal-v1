import request from './request';
import qs from 'qs';

const customers = ({ startDate, endDate }: { startDate: string, endDate: string }) => {
    const query = {
        start_date: startDate,
        end_date: endDate
    }
    return request.get(
        `/api/analytics/customers?` + qs.stringify(query),
    );
}

const getManagementData = async () => {
    const { data } = await request.get('/api/payment/transactions/search?limit=50000&sub_type=' +
        'refer_a_friend,referral_code,registration',
    );
    return data.transactions.filter((item: any) => item.created_at)
}

export {
    getManagementData,
    customers
}