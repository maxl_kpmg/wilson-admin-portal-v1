import numeral from 'numeral';

const amountFormatter = (cell: string) => {
  if (cell === '-') return '<strong>-</strong>'; // please change to a more accepted format
  if (typeof cell === 'undefined' || cell === '') {
    return cell;
  }
  let amount = numeral(cell).format('(0,0)');
  return amount;
};

export { amountFormatter };
