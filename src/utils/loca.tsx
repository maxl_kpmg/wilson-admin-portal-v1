import { isObject } from 'lodash';

const setStorage = (key: string, value: any) => {
  let newValue = value;
  if (isObject(value)) {
    newValue = JSON.stringify(value);
  }
  localStorage.setItem(key, newValue);
};

const getStorage = (key: string, isParse: boolean = false) => {
  const value = localStorage.getItem(key);

  if (!value) {
    return null;
  }

  if (isParse) {
    return JSON.parse(value);
  }

  return value;
};

const reomveStorage = (key: string) => {
  localStorage.removeItem(key);
};

export { setStorage, getStorage, reomveStorage };
