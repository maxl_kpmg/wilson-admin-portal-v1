const downloadFile = (blob: Blob, filename: string) => {
    const url = window.URL.createObjectURL(
        new Blob(['\uFEFF', blob], { type: 'text/csv; charset=utf-8' }),
    );
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', filename);

    document.body.appendChild(link);

    // Start download
    link.click();

    link?.parentNode?.removeChild(link);
}


export { amountFormatter } from './formatter'
export { setStorage, getStorage, reomveStorage } from './loca'
export {
    downloadFile
}