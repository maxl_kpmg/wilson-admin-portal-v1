const statusMapping = new Map([
    ['active', '#04ba82'],
    ['inactive', 'red']
])
const tierMapping = new Map([
    ['tier_1', { value: 'Classic', color: 'green' }],
    ['tier_2', { value: 'Prestige', color: 'cyan' }],
]);
export {
    statusMapping, tierMapping
}