import { useCallback, useEffect, useState } from 'react';
import _ from 'lodash';

interface IResult {
    list: any[],
    total: number
}

const useTable = (callback: (data?: any, key?: any) => Promise<(IResult)>) => {
    const [loading, setLoading] = useState(false)
    const [tableData, setTableData] = useState<IResult>({ list: [], total: 0 })
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 10,
    })

    const init = async (value?: string) => {
        try {
            setLoading(true)
            const result = await callback(pagination, { key: value })
            setTableData(result)
        } catch (error) { } finally {
            setLoading(false)
        }
    }

    const handleChange = useCallback((data: any) => {
        setPagination(data)
    }, [])

    useEffect(() => {
        init()
    }, [pagination.current])

    const handleSearch = _.debounce((value: string) => {
        init(value)
    }, 1200)


    return {
        tableProps: {
            dataSource: tableData.list,
            loading: loading,
            onChange: handleChange,
            pagination: {
                ...pagination,
                total: tableData.total,
            }
        },
        handleSearch
    }
}

export {
    useTable,
    IResult
}