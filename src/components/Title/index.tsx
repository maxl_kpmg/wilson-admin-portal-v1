import { FC } from 'react';
import './index.less';

interface TitleProps {
  value: string;
}
const Title: FC<TitleProps> = ({ value }) => {
  return <div className="wil_title">{value}</div>;
};

export default Title;
