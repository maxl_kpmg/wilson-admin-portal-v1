import { Row, Col } from 'antd';
import { FC } from 'react';

interface ItemProps {
  label: string;
  value?: string;
  labelSpan?: number;
  valueSpan?: number;
}

const Item: FC<ItemProps> = ({ label, children, labelSpan, valueSpan }) => {
  return (
    <Row>
      <Col span={labelSpan ? labelSpan : 11}>
        <label className="wil_bold">{label}</label>
      </Col>
      <Col span={valueSpan ? valueSpan : 12}>{children}</Col>
    </Row>
  );
};

export default Item;
