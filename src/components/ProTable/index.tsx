import React, { FC, useMemo, useState } from 'react';
import { Table, Button, Input, Drawer } from 'antd';
import { useTable } from '@/hooks';
import { useToggle } from 'ahooks';
import {
  DownloadOutlined,
  SearchOutlined,
  SettingOutlined,
} from '@ant-design/icons';
import { IResult } from '@/hooks/useTable';
import { ColumnsType, TableProps } from 'antd/es/table';
import ColumnsSetting from './ColumnsSetting';
import _ from 'lodash';

interface ProTableProps {
  exportFn: () => Promise<any>;
  columnList: any[];
  loadingTableData: (data: any, query: any) => Promise<IResult>;
  isExport?: boolean;
  isSearch?: boolean;
  optionProps?: TableProps<any>;
}

const ProTable: FC<ProTableProps> = ({
  exportFn,
  columnList,
  loadingTableData,
  isExport,
  isSearch,
  optionProps,
}) => {
  const [loading, setLoading] = useState(false);
  const [visible, { setLeft, setRight }] = useToggle(false);
  const { tableProps, handleSearch } = useTable(loadingTableData);
  const [key, setKey] = useState();
  const [colKeys, setColKeys] = useState<React.Key[]>(
    columnList.map((x) => x.key) as React.Key[],
  );
  const [columns, setColumns] = useState(columnList);

  const handlerExport = async () => {
    setLoading(true);
    await exportFn();
    setLoading(false);
  };

  const handleColumns = useMemo<ColumnsType<any> | undefined>(() => {
    let newData = _.cloneDeep(columns);
    newData.forEach((item, index) => {
      if (!item.children) return;
      item.children = item.children.filter((x: any) => !x.isShow);
      if (!item.children.length) {
        delete newData[index];
      }
    });
    return newData;
  }, [colKeys]);

  const handleColumnsSettingChange = (keys: React.Key[]) => {
    columns.forEach((item: any) => {
      if (!item.children || !item.title) return;
      item.children = item.children.map((x: any) => ({
        ...x,
        isShow: !keys.includes(x.key),
      }));
    });
    setColumns(columns);
    setColKeys(keys);
  };

  return (
    <div className="wil_table" style={{ marginTop: 50 }}>
      <Drawer title="Setting ColumnList" visible={visible} onClose={setLeft}>
        <ColumnsSetting
          onChange={handleColumnsSettingChange}
          data={columns}
          defaultData={colKeys.filter(Boolean)}
        />
      </Drawer>
      <div className="wil_table_header">
        <div className="wil_table_action">
          <div className="wil_table_search">
            {isSearch ? (
              <Input
                value={key}
                onChange={(e) => {
                  setKey(key);
                  handleSearch(e.target.value);
                }}
                prefix={<SearchOutlined />}
              ></Input>
            ) : null}
          </div>
          <div>
            {isExport ? (
              <div className="wil_table_action_item">
                <Button
                  type="primary"
                  loading={loading}
                  onClick={handlerExport}
                  icon={<DownloadOutlined />}
                >
                  Export CSV
                </Button>
              </div>
            ) : null}
          </div>
        </div>
        <Button
          type="link"
          onClick={setRight}
          icon={<SettingOutlined />}
        ></Button>
      </div>

      <Table
        columns={handleColumns}
        style={{ padding: 20 }}
        rowKey="key"
        {...tableProps}
        scroll={{ x: 'calc(700px + 50%)', y: 800 }}
        bordered
        {...optionProps}
      ></Table>
    </div>
  );
};

export default ProTable;
