import { useState, FC } from 'react';
import { Row, Col, Card, Form, Input, Button, message } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import logo from '@/assets/img/logo-wil-dark.png';
import { login, IAuth } from '@/api/auth';
import { history, AuthModelState, ConnectProps, Loading, connect } from 'umi';

import './login.less';

interface LoginProps extends ConnectProps {
  auth: AuthModelState;
}

const Login: FC<LoginProps> = ({ auth, dispatch }) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);
  console.log(auth);

  const handleSubmit = async (formData: IAuth) => {
    try {
      setLoading(true);
      const { data } = await login(formData);
      dispatch!({ type: 'auth/setToken', payload: data.token });
      history.push('/');
      setLoading(false);
    } catch (error) {
      message.error('网络错误');
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="login_page">
      <Row>
        <Col offset={15}>
          <Card>
            <div className="login_card">
              <div className="login_logo">
                <img alt="logo" src={logo} />
              </div>
              <div className="login_form">
                <Form form={form} onFinish={handleSubmit}>
                  <h1>Login</h1>
                  <p>Sign in to your account</p>
                  <Form.Item
                    name="email"
                    rules={[
                      {
                        required: true,
                        message: 'Please input your email!',
                      },
                    ]}
                  >
                    <Input
                      prefix={<UserOutlined className="site-form-item-icon" />}
                    />
                  </Form.Item>
                  <Form.Item
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: 'Please input your password!',
                      },
                    ]}
                  >
                    <Input.Password
                      prefix={<LockOutlined className="site-form-item-icon" />}
                    />
                  </Form.Item>
                  <div className="login_submit">
                    <Button type="link">Forgot Password?</Button>
                    <Button
                      loading={loading}
                      type="primary"
                      size="large"
                      htmlType="submit"
                    >
                      Login
                    </Button>
                  </div>
                </Form>
              </div>
            </div>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default connect(({ auth }: { auth: AuthModelState }) => {
  return {
    auth,
  };
})(Login);
