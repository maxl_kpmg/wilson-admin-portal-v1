

export interface Expiry {
    date: Date;
    amount: number;
}

export interface Balances {
    burn: number;
    earn: number;
    debit?: any;
    total: number;
    credit?: any;
    expiry: Expiry[];
    voucher: number;
    referrals: number;
}

export interface Identity {
}

export interface Profile {
    key: string;
    email: string;
    gender: string;
    mobile: string;
    district: string;
    interests: string[];
    last_name: string;
    first_name: string;
    date_of_birth: string;
    referral_code: string;
    is_sms_enabled: boolean;
    is_push_enabled: boolean;
    first_login_flag: boolean;
    is_email_enabled: boolean;
    third_party_data: boolean;
    years_of_driving: string;
    zurich_insurance: boolean;
    zurich_marketing: boolean;
    preferred_language: string;
    octopus_card_number: any[];
    zurich_insurance_time: string;
}


export interface Properties {
    tier: string;
    status: string;
    membershipId: string;
    status_start: Date;
    status_expiry: Date;
    expiry: Expiry[];
}

export interface IMember {
    id: string;
    status: string;
    email: string;
    balances: Balances;
    identity: Identity;
    profile: Profile;
    properties: Properties;
    createdAt: Date;
    updatedAt: Date;
    version: number;
    name?: string;
    tier?: string;
    status_expiry?: string;
    points?: number;
    expiry_1?: number;
    expiry_2?: number;
    expiry_3?: number;
    expiry_4?: number;
}



