import { FC, useEffect, useState } from 'react';
import { Card } from 'antd';
import { Line } from 'react-chartjs-2';
import numeral from 'numeral';
import { ChartData } from 'chart.js';
import moment from 'moment';
import { customers, getManagementData } from '@/api/chart';
import _ from 'lodash';

const defaultOptions = {
  pointDot: true,
  pointDotRadius: 4,
  pointDotStrokeWidth: 1,
  pointHitDetectionRadius: 20,
  datasetStroke: true,
  datasetStrokeWidth: 2,
  datasetFill: false,

  maintainAspectRatio: false,

  plugins: {
    legend: {
      display: false,
    },
  },
  scales: {
    xAxes: {
      display: false,
      gridLines: {
        color: 'transparent',
        zeroLineColor: 'transparent',
      },
      ticks: {
        fontSize: 2,
        fontColor: 'transparent',
      },
      barPercentage: 0.6,
    },
    yAxes: {
      display: false,
      ticks: {
        display: false,
        min: 0,
      },
    },
  },
};

interface IAverageUsers {
  data: number[];
  labels: string[];
}

// interface LineChartProps {
//   options: any;
//   height: number;
//   data: ChartData<'line'> & { total: number };
// }

const LineChart: FC<{}> = (props) => {
  const [chartData, setChartData] = useState<IAverageUsers>({
    data: [],
    labels: [],
  });
  const options: any = {
    ...defaultOptions,
  };

  const getChartData = async () => {
    const startDate = moment()
      .subtract(1, 'year')
      .format('YYYY-MM-DD HH:mm:ss');
    const endDate = moment().format('YYYY-MM-DD HH:mm:ss');
    const averageUsersData = await customers({ startDate, endDate });
    const averageUsers = _.get(averageUsersData, 'data.average_users', {
      data: [],
      labels: [],
    });

    setChartData(averageUsers);
  };

  const handleAverageUsersData = ({
    data,
    labels,
  }: IAverageUsers): ChartData<'line'> & { total: number } => {
    const average = _.sum(data) / data.length;
    return {
      total: average,
      labels: labels,
      datasets: [
        {
          backgroundColor: '#ffffff',
          borderColor: '#007BFF',
          data: data,
        },
      ],
    };
  };

  useEffect(() => {
    getChartData();
  }, []);

  const { total, ...data } = handleAverageUsersData(chartData);

  return (
    <Card title="AVERAGE DAILY ACTIVE USERS">
      <h1 style={{ fontSize: 40 }}>{numeral(total).format('0,0')}</h1>
      <div style={{ height: 75 }}>
        <Line data={data} options={options} />
      </div>
    </Card>
  );
};

export default LineChart;
