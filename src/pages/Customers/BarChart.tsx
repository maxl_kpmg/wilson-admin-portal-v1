import { FC, useEffect, useState } from 'react';
import { Card, Row, Col } from 'antd';
import { Bar } from 'react-chartjs-2';

import { getManagementData } from '@/api/chart';
import _ from 'lodash';
import numeral from 'numeral';
import moment from 'moment';

type EColor = 'referral' | 'signUps';

const colorMapping = {
  referral: {
    backgroundColor: '#F86C6B',
    borderColor: 'rgba(255,255,255,.55)',
  },
  signUps: {
    backgroundColor: '#FFD68E',
    borderColor: 'rgba(255,255,255,.55)',
  },
};

const defaultOptions: any = {
  pointDot: true,
  pointDotRadius: 4,
  pointDotStrokeWidth: 1,
  pointHitDetectionRadius: 20,
  datasetStroke: true,
  datasetStrokeWidth: 2,
  datasetFill: false,

  maintainAspectRatio: false,
  tooltips: {
    enabled: false,
  },

  plugins: {
    legend: {
      display: false,
    },
    title: {
      display: false,
    },
  },
  scales: {
    xAxes: {
      display: false,
      gridLines: {
        color: 'transparent',
        zeroLineColor: 'transparent',
      },
      ticks: {
        fontSize: 2,
        fontColor: 'transparent',
      },
      barPercentage: 0.6,
    },
    yAxes: {
      display: false,
    },
  },
  elements: {
    line: {
      borderWidth: 2,
    },
    point: {
      radius: 4,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
};

const conver = (data: { created_at: string; subType: string }[]) => {
  const result = _.groupBy(
    data.map((x) => ({
      date: x.created_at.substring(0, 10),
      subType: x.subType,
    })),
    (x) => x.subType,
  );
  const referral = _.groupBy(
    [
      ..._.get(result, 'refer_a_friend', []),
      ..._.get(result, 'referral_code', []),
    ],
    (x) => x.date,
  );

  const signUps = _.groupBy(_.get(result, 'registration', []), (x) => x.date);

  return _.mapValues({ referral, signUps }, (item, index: EColor) => {
    const values = Object.values(item).map((x) => x.length);

    return {
      labels: Object.keys(item),
      datasets: [
        {
          data: values,
          ...colorMapping[index],
        },
      ],
      total: values.reduce((x, y) => x + y, 0),
    };
  });
};

const initState = {
  referral: {
    labels: [],
    datasets: [],
    total: 0,
  },
  signUps: {
    labels: [],
    datasets: [],
    total: 0,
  },
};

const BarChart = () => {
  const [chartData, setChartData] = useState<any>(initState);

  const getChartData = async () => {
    const result = await getManagementData();

    setChartData(conver(result));
  };

  useEffect(() => {
    getChartData();
  }, []);

  return (
    <Row className="wil_bar_chart" gutter={8}>
      <Col span={12}>
        <Card>
          <h1 style={{ fontSize: 40 }}>
            {numeral(chartData.referral.total).format('0,0')}
          </h1>
          <span>Referral</span>
          <div style={{ height: 75 }}>
            <Bar data={chartData.referral} options={defaultOptions} />
          </div>
        </Card>
      </Col>
      <Col span={12}>
        <Card>
          <h1 style={{ fontSize: 40 }}>
            {numeral(chartData.signUps.total).format('0,0')}
          </h1>
          <span>Sign-Ups</span>
          <div style={{ height: 75 }}>
            <Bar data={chartData.signUps} options={defaultOptions} />
          </div>
        </Card>
      </Col>
    </Row>
  );
};

export default BarChart;
