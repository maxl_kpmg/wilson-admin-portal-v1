import { Card, Skeleton, Button } from 'antd';
import { CardTabListType } from 'antd/es/card';
import { FC, useEffect, useState } from 'react';
import PersonalInfo from './PersonalInfo';
import { getCustomerById } from '@/api/customers';
import { IMember } from '../model';
import './index.less';
import { tierMapping } from '@/constants';
import moment from 'moment';

interface DetailsProps {
  row: IMember;
}

type tab = 'PersonalInfo';

const tabListNoTitle: CardTabListType[] = [
  {
    key: 'PersonalInfo',
    tab: 'PersonalInfo',
  },
  {
    key: 'CarInformation',
    tab: 'Car Information',
  },
  {
    key: 'MembershipTierMovement',
    tab: 'Membership Tier Movement',
  },
];

const Details: FC<DetailsProps> = ({ row }) => {
  const [customers, setCustomers] = useState<IMember>();
  const [activeTabKey, setActiveTabKey] = useState<string>('PersonalInfo');
  const [loading, setLoading] = useState(true);
  const formtterData = (data: IMember) => {
    const { profile, properties } = data;
    return {
      ...data,
      name: profile.last_name + ' ' + profile.first_name,
      tier: tierMapping.get(properties.tier)?.value + ' ' + 'Member',
      status_expiry:
        'Expire on' +
        ' ' +
        moment(properties.status_expiry).format('DD MMM YYYY'),
      points: properties.expiry.reduce((x, y) => x + y.amount, 0),
      expiry_1: properties.expiry[0].amount,
      expiry_2: properties.expiry[1].amount,
      expiry_3: properties.expiry[2].amount,
      expiry_4: properties.expiry[3].amount,
    };
  };

  const initData = async () => {
    const { data } = await getCustomerById(row.id);
    setCustomers(formtterData(data));
    setLoading(false);
  };

  useEffect(() => {
    initData();
  }, []);

  const contentList: { [index: string]: JSX.Element } = {
    PersonalInfo: <PersonalInfo customers={customers} />,
    CarInformation: <PersonalInfo customers={customers} />,
    MembershipTierMovement: <PersonalInfo customers={customers} />,
  };

  return loading ? (
    <Skeleton />
  ) : (
    <div className="wils_member_detail">
      <div className="wils_member_detail_header">
        <h1 style={{ fontSize: 50 }}>{customers?.name}</h1>
        <span>{customers?.tier}</span>
        <span className="wil_bold">{customers?.status_expiry}</span>
      </div>
      <Card
        tabList={tabListNoTitle}
        activeTabKey={activeTabKey}
        onTabChange={setActiveTabKey}
      >
        {contentList[activeTabKey]}
      </Card>
      <div className="w_1">
        <Button type="primary"></Button>
      </div>
    </div>
  );
};

export default Details;
