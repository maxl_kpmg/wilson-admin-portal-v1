import { Row, Col, Space, Card, Switch } from 'antd';
import { FC } from 'react';
import { IMember, Profile } from '../model';
import _ from 'lodash';
import moment from 'moment';
import Item from '@/components/Item';

export interface PersonalInfoProps {
  customers: IMember | undefined;
}

const PersonalInfoAction: FC<{ profile: Profile | undefined }> = ({
  profile,
}) => {
  return (
    <Card>
      <Space direction="vertical" style={{ width: '100%' }} size={22}>
        <Item labelSpan={15} valueSpan={9} label="Phone">
          <Switch defaultChecked={profile?.is_push_enabled} />
        </Item>
        <Item labelSpan={15} valueSpan={9} label="Email">
          <Switch defaultChecked={profile?.is_email_enabled} />
        </Item>
        <Item labelSpan={15} valueSpan={9} label="Third Parties">
          <Switch defaultChecked={profile?.zurich_marketing} />
        </Item>
        <Item labelSpan={15} valueSpan={9} label="Zurich Direct Marketing">
          <Switch defaultChecked={profile?.zurich_insurance} />
        </Item>
        <Item
          labelSpan={15}
          valueSpan={9}
          label="Zurich Free Motor Vehicle personal property Protection"
        >
          <Switch defaultChecked />
        </Item>
      </Space>
    </Card>
  );
};

const PersonalInfo: FC<PersonalInfoProps> = ({ customers }) => {
  return (
    <div className="wil_content">
      <Space direction="vertical" style={{ width: '100%' }} size={22}>
        <Item label="Member ID">
          <span>{customers?.profile.key}</span>
        </Item>
        <Item label="Gender">
          <span>{_.capitalize(customers?.profile.gender)}</span>
        </Item>
        <Item label="District">
          <span>{_.startCase(customers?.profile.district)}</span>
        </Item>
        <Item label="Date of Birth">
          <span>{customers?.profile.date_of_birth}</span>
        </Item>
        <Item label="Mobile">
          <span>{customers?.profile.mobile}</span>
        </Item>
        <Item label="Email">
          <span>{customers?.profile.email}</span>
        </Item>
        <Item label="Referral Code">
          <span>{customers?.profile.referral_code}</span>
        </Item>
        <Item label="Date Registered">
          <span>{moment(customers?.createdAt).format('YYYY-MM-DD')}</span>
        </Item>
        <Card>
          <div className="w">
            <h1 style={{ fontSize: 50 }}>{customers?.points}</h1>
            <span>WIL Points</span>
          </div>
        </Card>
        <Item label="Expiring This Year">
          <span>{customers?.expiry_1}</span>
        </Item>
        <Item label="Expiring in 1 Year">
          <span>{customers?.expiry_2}</span>
        </Item>
        <Item label="Expiring in 2 Year">
          <span>{customers?.expiry_3}</span>
        </Item>
        <Item label="Expiring in 3 Year">
          <span>{customers?.expiry_4}</span>
        </Item>
        <PersonalInfoAction profile={customers?.profile} />
        <Item label="Interest">
          <span>
            {customers?.profile.interests.map((x) => {
              return (
                <>
                  <span>{_.startCase(x)}</span>
                  <br />
                  <br />
                </>
              );
            })}
          </span>
        </Item>
      </Space>
    </div>
  );
};

export default PersonalInfo;
