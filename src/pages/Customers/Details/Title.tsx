import { FC } from 'react';

interface TitleProps {
  status: string;
  title: string;
}
const Title: FC<TitleProps> = ({ status, title }) => {
  return (
    <div className="wil_detail_title">
      <span>{title}</span>
      <span>{status}</span>
    </div>
  );
};

export default Title;
