import { Tree } from 'antd';
import { ColumnsType } from 'antd/es/table';
import React, { FC } from 'react';

interface ColumnsSettingProps {
  data: any[];
  defaultData: React.Key[];
  onChange: (keys: React.Key[]) => void;
}

const columnsList = ['key', 'name', 'createdAt', 'status'];

const ColumnsSetting: FC<ColumnsSettingProps> = ({
  data,
  defaultData,
  onChange,
}) => {
  const treeData = data.filter((x) => !columnsList.includes(x.key) && x.key);

  const onCheck = (checkedKeys: any) => onChange(checkedKeys);

  return (
    <Tree
      onCheck={onCheck}
      defaultCheckedKeys={defaultData.filter(
        (x: any) => !columnsList.includes(x),
      )}
      checkable
      defaultExpandAll
      treeData={treeData}
    />
  );
};

export default ColumnsSetting;
