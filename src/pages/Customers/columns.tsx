import { ColumnsType } from 'antd/es/table';
import { Tooltip, Tag } from 'antd';
import _ from 'lodash';
import { amountFormatter } from '@/utils';
import { statusMapping, tierMapping } from '@/constants';
import moment from 'moment';

const toggleOption = (value: boolean) =>
  value === true ? 'opt-in' : 'opt-out';

const personalInfo = [
  {
    title: 'Email',
    dataIndex: 'email',
    align: 'center',
    ellipsis: {
      showTitle: false,
    },
    render: (cell: string) => (
      <Tooltip placement="topLeft" title={cell}>
        {cell}
      </Tooltip>
    ),
  },
  {
    title: 'Membership Tier',
    dataIndex: 'tier',
    align: 'center',
    key: 'tier',
    render: (cell: string) => {
      const tier = tierMapping.get(cell);
      return <Tag color={tier?.color}> {tier?.value} </Tag>;
    },
  },
  {
    title: 'DOB',
    dataIndex: 'date_of_birth',
    align: 'center',
    key: 'date_of_birth',
  },
  {
    title: 'Gender',
    dataIndex: 'gender',
    align: 'center',
    key: 'gender',
    render: (cell: string) => _.capitalize(cell),
  },
  {
    title: 'District',
    dataIndex: 'district',
    align: 'center',
    key: 'district',
    render: _.startCase,
  },
  {
    title: 'Mobile',
    dataIndex: 'mobile',
    align: 'center',
    key: 'mobile',
  },
  {
    title: 'Friend Referral',
    dataIndex: 'referral_code',
    align: 'center',
    key: 'referral_code',
  },
  {
    title: 'Interest',
    dataIndex: 'interests',
    align: 'center',
    key: 'interests',
    ellipsis: {
      showTitle: false,
    },
    render: (cell: string[] | null) => {
      const value = cell
        ? cell.map((x: string) => _.startCase(x)).join(',')
        : '';
      return (
        <Tooltip placement="topLeft" title={value}>
          {value}
        </Tooltip>
      );
    },
  },
].map((x) => ({ width: 200, ...x, key: x.dataIndex })) as ColumnsType<any>;

const subscriptions = [
  {
    title: 'Phone',
    dataIndex: 'is_push_enabled',
    align: 'center',
    key: 'is_push_enabled',
    render: toggleOption,
  },
  {
    title: 'Email',
    dataIndex: 'is_email_enabled',
    align: 'center',
    key: 'is_email_enabled',
    render: toggleOption,
  },
  {
    title: 'Message',
    dataIndex: 'is_sms_enabled',
    align: 'center',
    key: 'is_sms_enabled',
    render: toggleOption,
  },
  {
    title: 'Third Parties',
    dataIndex: 'third_party_data',
    align: 'center',
    key: 'third_party_data',
    render: toggleOption,
  },
  {
    title: 'Zurich Direct Marketing',
    dataIndex: 'zurich_marketing',
    align: 'center',
    key: 'zurich_marketing',
    render: toggleOption,
  },
  {
    title: 'Zurich Free Motor Vehicle personal property Protection',
    dataIndex: 'zurich_insurance',
    align: 'center',
    key: 'zurich_insurance',
    ellipsis: true,
    render: toggleOption,
  },
].map((x) => ({ width: 200, ...x, key: x.dataIndex })) as ColumnsType<any>;

const points = [
  {
    title: 'Total Points Balance',
    dataIndex: 'balance',
    align: 'center',
    key: 'balance',
  },
  {
    title: 'Earn',
    dataIndex: 'coins_earned',
    align: 'center',
    key: 'coins_earned',
  },
  {
    title: 'Burn',
    dataIndex: 'coins_burned',
    align: 'center',
    key: 'coins_burned',
  },
  {
    title: 'Expire This Year',
    dataIndex: 'expires_0',
    align: 'center',
    key: 'expires_0',
  },
  {
    title: 'Expire in 1 Year',
    dataIndex: 'expires_1',
    align: 'center',
    key: 'expires_1',
  },
  {
    title: 'Expire in 2 Year',
    dataIndex: 'expires_2',
    align: 'center',
    key: 'expires_2',
  },
  {
    title: 'Expire in 3 Year',
    dataIndex: 'expires_3',
    align: 'center',
    key: 'expires_3',
  },
].map((x) => ({
  ...x,
  render: amountFormatter,
  width: 200,
  key: x.dataIndex,
})) as ColumnsType<any>;

const carInfo = [
  {
    title: 'Octopus Card Number 1',
    dataIndex: 'octopus_card_number_0',
    align: 'center',
    key: 'octopus_card_number_0',
  },
  {
    title: 'Octopus Card Number 2',
    dataIndex: 'octopus_card_number_1',
    align: 'center',
    key: 'octopus_card_number_1',
  },
  {
    title: 'Octopus Card Number 3',
    dataIndex: 'octopus_card_number_2',
  },
  {
    title: 'Years of Driving',
    dataIndex: 'years_of_driving',
  },
  {
    dataIndex: 'car_plate_number_0',
    title: 'Car Plate Number' + ' 1',
  },
  {
    dataIndex: 'car_brand_0',
    title: 'Car Brand' + ' 1',
    render: _.startCase,
  },
  {
    dataIndex: 'car_model_0',
    title: 'Car Model' + ' 1',
  },
  {
    dataIndex: 'purchase_date_0',
    title: 'Purchase date' + ' 1',
  },
  {
    dataIndex: 'car_plate_number_1',
    title: 'Car Plate Number' + ' 2',
  },
  {
    dataIndex: 'car_brand_1',
    title: 'Car Brand' + ' 2',
    render: _.startCase,
  },
  {
    dataIndex: 'car_model_1',
    title: 'Car Model' + ' 2',
  },
  {
    dataIndex: 'purchase_date_1',
    title: 'Purchase date' + ' 2',
  },
  {
    dataIndex: 'car_plate_number_2',
    title: 'Car Plate Number' + ' 3',
  },
  {
    dataIndex: 'car_brand_2',
    title: 'Car Brand' + ' 3',
    render: _.startCase,
  },
  {
    dataIndex: 'car_model_2',
    title: 'Car Model' + ' 3',
  },
  {
    dataIndex: 'purchase_date_2',
    title: 'Purchase date' + ' 3',
  },
].map((x) => ({
  ...x,
  width: 200,
  align: 'center',
  key: x.dataIndex,
})) as ColumnsType<any>;

const others: ColumnsType<any> = [
  {
    title: 'Date Added',
    dataIndex: 'createdAt',
    render: (cell: string) => moment(cell).format('YYYY-MM-DD'),
  },
  {
    title: 'Status',
    dataIndex: 'status',
    render: (cell: string) => {
      const color = statusMapping.get(cell);
      return <Tag color={color}>{_.capitalize(cell)}</Tag>;
    },
  },
].map((x) => ({ width: 200, ...x, align: 'center' }));

export { personalInfo, subscriptions, points, carInfo, others };
