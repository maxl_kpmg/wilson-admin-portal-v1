import Title from '@/components/Title';
import { Dropdown, Menu, Modal } from 'antd';
import { getCustomerList, exportCustomerCSV } from '@/api/customers';
import { IQuery } from '@/api/global';
import { FC, useState } from 'react';
import { useToggle } from 'ahooks';
import { DownOutlined } from '@ant-design/icons';
import _ from 'lodash';
import { ColumnsType } from 'antd/es/table';
import {
  personalInfo,
  subscriptions,
  points,
  carInfo,
  others,
} from './columns';
import { IResult } from '@/hooks/useTable';
import LineChart from './LineChart';
import BarChart from './BarChart';
import Details from './Details';
import ProTable from '@/components/ProTable';
import ModalTitle from './Details/Title';
import { IMember } from './model';

const dataFormatter = (x: any) => {
  if (x.properties && x.properties.expiry) {
    const entries = x.properties.expiry;
    x.expires_0 = entries[0] ? entries[0].amount || 0 : 0;
    x.expires_1 = entries[1] ? entries[1].amount || 0 : 0;
    x.expires_2 = entries[2] ? entries[2].amount || 0 : 0;
    x.expires_3 = (entries[3] || 0) > 0 ? entries[3] : x.balances.total;
  }
  x.coins_earned = x.balances.earn ? x.balances.earn : 0;
  x.coins_burned = x.balances.burn ? x.balances.burn : 0;
  x.coins_credit = x.balances.credit ? x.balances.credit : 0;
  x.coins_debit = x.balances.debit ? x.balances.debit : 0;
  x.balance = x.balances.total ? x.balances.total : 0;

  _.get(x, 'cars', []).forEach((item: any, index: number) => {
    x[`car_plate_number_${index}`] = item.properties.license_plate;
    x[`car_brand_${index}`] = item.properties.brand;
    x[`car_model_${index}`] = item.properties.model;
    x[`purchase_date_${index}`] = item.properties.purchase_date;
  });

  let octopus_card_number = _.get(x, 'profile.octopus_card_number', []);
  octopus_card_number = octopus_card_number ? octopus_card_number : [];
  octopus_card_number = Array.isArray(octopus_card_number)
    ? octopus_card_number
    : [octopus_card_number];

  octopus_card_number.forEach((item: string, index: number) => {
    x[`octopus_card_number_${index}`] = item;
  });

  return {
    ...x.profile,
    ...x.properties,
    ...x,
  };
};

const getTableData = async (pagination: any, query: any): Promise<IResult> => {
  const { pageSize, current } = pagination;

  const params: IQuery = {
    limit: pageSize,
    offset: (current - 1) * pageSize,
    sort: '',
    dir: '',
    ...query,
  };
  const { data } = await getCustomerList(params);

  return {
    total: data.count,
    list: data.results.map(dataFormatter),
  };
};

const ManageMembers: FC = (props) => {
  const [row, setRow] = useState<IMember>();
  const [visible, { setLeft: setLeft, setRight: setRight }] = useToggle(false);
  const columnList: ColumnsType<any> = [
    {
      title: 'Member ID',
      dataIndex: 'key',
      key: 'key',
      align: 'center',
      fixed: 'left',
      width: 300,
      onCell: (row) => {
        return {
          onClick: () => {
            setRow(row);
            setRight();
          },
        };
      },
    },
    {
      title: 'Member Name',
      dataIndex: 'name',
      key: 'name',
      align: 'center',
      fixed: 'left',
      width: 200,
      render: (cell: string, row: any) =>
        _.get(row, 'last_name', '') + ' ' + _.get(row, 'first_name', ''),
    },
    {
      title: 'Personal Info',
      key: 'personal_info',
      align: 'center',
      children: personalInfo,
    },
    {
      title: 'Manage Subscriptions',
      key: 'manage_subscriptions',
      align: 'center',
      children: subscriptions,
    },
    {
      title: 'WIL Points',
      key: 'wil_points',
      align: 'center',
      children: points,
    },
    {
      title: 'Car Info',
      key: 'car_info',
      align: 'center',
      children: carInfo,
    },
    {
      title: '',
      dataIndex: 'createdAt',
      width: 200,
      children: [
        ...others,
        {
          title: 'Action',
          width: 200,
          align: 'center',
          render: () => {
            const menu = (
              <Menu>
                <Menu.Item key="1">Cancel Membership</Menu.Item>
                <Menu.Item key="2">Deactivate Membership</Menu.Item>
                <Menu.Item key="3">Reactivate Member</Menu.Item>
              </Menu>
            );
            return (
              <Dropdown overlay={menu}>
                <a
                  className="ant-dropdown-link"
                  onClick={(e) => e.preventDefault()}
                >
                  更多 <DownOutlined />
                </a>
              </Dropdown>
            );
          },
        },
      ],
    },
  ];
  return (
    <div className="members">
      <Modal
        width={800}
        destroyOnClose={true}
        title={[
          <ModalTitle
            key="1"
            title="ACCOUNT SUMMARY"
            status={_.capitalize(row?.status)}
          />,
        ]}
        visible={visible}
        onCancel={setLeft}
      >
        <Details row={row!} />
      </Modal>
      <Title value="MANAGE MEMBERS" />
      <LineChart />
      <BarChart />
      <ProTable
        isExport={true}
        isSearch={true}
        columnList={columnList}
        loadingTableData={getTableData}
        exportFn={exportCustomerCSV}
      />
    </div>
  );
};

export default ManageMembers;
