webpackHotUpdate("umi",{

/***/ "./src/pages/Members/BarChart.tsx":
/*!****************************************!*\
  !*** ./src/pages/Members/BarChart.tsx ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__) {/* harmony import */ var _Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@umijs/babel-preset-umi/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/@umijs/babel-preset-umi/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@umijs/babel-preset-umi/node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./node_modules/@umijs/babel-preset-umi/node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var _Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@umijs/babel-preset-umi/node_modules/@babel/runtime/regenerator */ "./node_modules/@umijs/babel-preset-umi/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-chartjs-2 */ "./node_modules/react-chartjs-2/dist/index.modern.js");
/* harmony import */ var _api_chart__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/api/chart */ "./src/api/chart.ts");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");




var _jsxFileName = "/Users/max.lv/Documents/code/wilson-admin-portal-v1/src/pages/Members/BarChart.tsx",
    _s = __webpack_require__.$Refresh$.signature();






var defaultOptions = {
  tooltips: {
    enabled: false
  },
  maintainAspectRatio: false,
  legend: {
    display: false
  },
  scales: {
    xAxes: [{
      gridLines: {
        color: 'transparent',
        zeroLineColor: 'transparent'
      },
      ticks: {
        fontSize: 2,
        fontColor: 'transparent'
      },
      barPercentage: 0.6
    }]
  },
  elements: {
    line: {
      borderWidth: 2
    },
    point: {
      radius: 4,
      hitRadius: 10,
      hoverRadius: 4
    }
  }
};

var BarChart = _ref => {
  _s();

  var data = _ref.data;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(),
      _useState2 = Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_1__["default"])(_useState, 2),
      chartData = _useState2[0],
      setChartData = _useState2[1];

  var getChartData = /*#__PURE__*/function () {
    var _ref2 = Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_0__["default"])( /*#__PURE__*/_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.mark(function _callee() {
      var result;
      return _Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return Object(_api_chart__WEBPACK_IMPORTED_MODULE_5__["getManagementData"])();

            case 2:
              result = _context.sent;
              console.log(result);

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getChartData() {
      return _ref2.apply(this, arguments);
    };
  }();

  Object(react__WEBPACK_IMPORTED_MODULE_3__["useEffect"])(() => {
    getChartData();
  }, []);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("div", {
    style: {
      height: 75
    },
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])(react_chartjs_2__WEBPACK_IMPORTED_MODULE_4__["Bar"], {
      data: data,
      options: defaultOptions
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 7
    }, undefined)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 54,
    columnNumber: 5
  }, undefined);
};

_s(BarChart, "LmsCyr6MQhwVasL7GTKlIjQ8Q1M=");

_c = BarChart;
/* harmony default export */ __webpack_exports__["default"] = (BarChart);

var _c;

__webpack_require__.$Refresh$.register(_c, "BarChart");

var $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
var $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	var errorOverlay;
	if (true) {
		errorOverlay = false;
	}
	var testMode;
	if (typeof __react_refresh_test__ !== 'undefined') {
		testMode = __react_refresh_test__;
	}
	return __react_refresh_utils__.executeRuntime(
		exports,
		$ReactRefreshModuleId$,
		module.hot,
		errorOverlay,
		testMode
	);
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/umi/node_modules/@umijs/preset-built-in/bundled/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/umi/node_modules/@umijs/preset-built-in/bundled/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js")))

/***/ }),

/***/ "./src/pages/Members/index.tsx":
/*!*************************************!*\
  !*** ./src/pages/Members/index.tsx ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__) {/* harmony import */ var antd_es_table_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! antd/es/table/style */ "./node_modules/antd/es/table/style/index.js");
/* harmony import */ var antd_es_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd/es/table */ "./node_modules/antd/es/table/index.js");
/* harmony import */ var antd_es_button_style__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd/es/button/style */ "./node_modules/antd/es/button/style/index.js");
/* harmony import */ var antd_es_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! antd/es/button */ "./node_modules/antd/es/button/index.js");
/* harmony import */ var antd_es_input_style__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! antd/es/input/style */ "./node_modules/antd/es/input/style/index.js");
/* harmony import */ var antd_es_input__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! antd/es/input */ "./node_modules/antd/es/input/index.js");
/* harmony import */ var antd_es_drawer_style__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! antd/es/drawer/style */ "./node_modules/antd/es/drawer/style/index.js");
/* harmony import */ var antd_es_drawer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! antd/es/drawer */ "./node_modules/antd/es/drawer/index.js");
/* harmony import */ var _Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/@umijs/babel-preset-umi/node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./node_modules/@umijs/babel-preset-umi/node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var antd_es_dropdown_style__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! antd/es/dropdown/style */ "./node_modules/antd/es/dropdown/style/index.js");
/* harmony import */ var antd_es_dropdown__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! antd/es/dropdown */ "./node_modules/antd/es/dropdown/index.js");
/* harmony import */ var antd_es_menu_style__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! antd/es/menu/style */ "./node_modules/antd/es/menu/style/index.js");
/* harmony import */ var antd_es_menu__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! antd/es/menu */ "./node_modules/antd/es/menu/index.js");
/* harmony import */ var _Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./node_modules/@umijs/babel-preset-umi/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/@umijs/babel-preset-umi/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./node_modules/@umijs/babel-preset-umi/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/@umijs/babel-preset-umi/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./node_modules/@umijs/babel-preset-umi/node_modules/@babel/runtime/regenerator */ "./node_modules/@umijs/babel-preset-umi/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _components_Title__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @/components/Title */ "./src/components/Title/index.tsx");
/* harmony import */ var _api_customers__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @/api/customers */ "./src/api/customers.ts");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var _ant_design_icons__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @ant-design/icons */ "./node_modules/@ant-design/icons/es/index.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var _columns__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./columns */ "./src/pages/Members/columns.tsx");
/* harmony import */ var _hooks__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @/hooks */ "./src/hooks/index.ts");
/* harmony import */ var _LineChart__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./LineChart */ "./src/pages/Members/LineChart.tsx");
/* harmony import */ var _BarChart__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./BarChart */ "./src/pages/Members/BarChart.tsx");
/* harmony import */ var _ColumnsSetting__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./ColumnsSetting */ "./src/pages/Members/ColumnsSetting.tsx");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");

















var _jsxFileName = "/Users/max.lv/Documents/code/wilson-admin-portal-v1/src/pages/Members/index.tsx",
    _s = __webpack_require__.$Refresh$.signature();














var dataFormatter = x => {
  if (x.properties && x.properties.expiry) {
    var entries = x.properties.expiry;
    x.expires_0 = entries[0] ? entries[0].amount || 0 : 0;
    x.expires_1 = entries[1] ? entries[1].amount || 0 : 0;
    x.expires_2 = entries[2] ? entries[2].amount || 0 : 0;
    x.expires_3 = (entries[3] || 0) > 0 ? entries[3] : x.balances.total;
  }

  x.coins_earned = x.balances.earn ? x.balances.earn : 0;
  x.coins_burned = x.balances.burn ? x.balances.burn : 0;
  x.coins_credit = x.balances.credit ? x.balances.credit : 0;
  x.coins_debit = x.balances.debit ? x.balances.debit : 0;
  x.balance = x.balances.total ? x.balances.total : 0;

  lodash__WEBPACK_IMPORTED_MODULE_20___default.a.get(x, 'cars', []).forEach((item, index) => {
    x["car_plate_number_".concat(index)] = item.properties.license_plate;
    x["car_brand_".concat(index)] = item.properties.brand;
    x["car_model_".concat(index)] = item.properties.model;
    x["purchase_date_".concat(index)] = item.properties.purchase_date;
  });

  var octopus_card_number = lodash__WEBPACK_IMPORTED_MODULE_20___default.a.get(x, 'profile.octopus_card_number', []);

  octopus_card_number = octopus_card_number ? octopus_card_number : [];
  octopus_card_number = Array.isArray(octopus_card_number) ? octopus_card_number : [octopus_card_number];
  octopus_card_number.forEach((item, index) => {
    x["octopus_card_number_".concat(index)] = item;
  });
  return Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_14__["default"])(Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_14__["default"])(Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_14__["default"])({}, x.profile), x.properties), x);
};

var getTableData = /*#__PURE__*/function () {
  var _ref = Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_13__["default"])( /*#__PURE__*/_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_15___default.a.mark(function _callee(data1, query) {
    var pageSize, current, params, _yield$getMemberList, data;

    return _Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_15___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            pageSize = data1.pageSize, current = data1.current;
            params = Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_14__["default"])({
              limit: pageSize,
              offset: (current - 1) * pageSize,
              sort: '',
              dir: ''
            }, query);
            _context.next = 4;
            return Object(_api_customers__WEBPACK_IMPORTED_MODULE_17__["getMemberList"])(params);

          case 4:
            _yield$getMemberList = _context.sent;
            data = _yield$getMemberList.data;
            return _context.abrupt("return", {
              total: data.count,
              list: data.results.map(dataFormatter)
            });

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function getTableData(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

var columnList = [{
  title: 'Member ID',
  dataIndex: 'key',
  key: 'key',
  align: 'center',
  fixed: 'left',
  width: 300
}, {
  title: 'Member Name',
  dataIndex: 'name',
  key: 'name',
  align: 'center',
  fixed: 'left',
  width: 200,
  render: (cell, row) => lodash__WEBPACK_IMPORTED_MODULE_20___default.a.get(row, 'last_name', '') + ' ' + lodash__WEBPACK_IMPORTED_MODULE_20___default.a.get(row, 'first_name', '')
}, {
  title: 'Personal Info',
  key: 'personal_info',
  align: 'center',
  children: _columns__WEBPACK_IMPORTED_MODULE_21__["personalInfo"]
}, {
  title: 'Manage Subscriptions',
  key: 'manage_subscriptions',
  align: 'center',
  children: _columns__WEBPACK_IMPORTED_MODULE_21__["subscriptions"]
}, {
  title: 'WIL Points',
  key: 'wil_points',
  align: 'center',
  children: _columns__WEBPACK_IMPORTED_MODULE_21__["points"]
}, {
  title: 'Car Info',
  key: 'car_info',
  align: 'center',
  children: _columns__WEBPACK_IMPORTED_MODULE_21__["carInfo"]
}, {
  title: '',
  dataIndex: 'createdAt',
  width: 200,
  children: [..._columns__WEBPACK_IMPORTED_MODULE_21__["others"], {
    title: 'Action',
    width: 200,
    align: 'center',
    render: () => {
      var menu = /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(antd_es_menu__WEBPACK_IMPORTED_MODULE_12__["default"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(antd_es_menu__WEBPACK_IMPORTED_MODULE_12__["default"].Item, {
          children: "Cancel Membership"
        }, "1", false, {
          fileName: _jsxFileName,
          lineNumber: 141,
          columnNumber: 15
        }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(antd_es_menu__WEBPACK_IMPORTED_MODULE_12__["default"].Item, {
          children: "Deactivate Membership"
        }, "2", false, {
          fileName: _jsxFileName,
          lineNumber: 142,
          columnNumber: 15
        }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(antd_es_menu__WEBPACK_IMPORTED_MODULE_12__["default"].Item, {
          children: "Reactivate Member"
        }, "3", false, {
          fileName: _jsxFileName,
          lineNumber: 143,
          columnNumber: 15
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 140,
        columnNumber: 13
      }, undefined);

      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(antd_es_dropdown__WEBPACK_IMPORTED_MODULE_10__["default"], {
        overlay: menu,
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])("a", {
          className: "ant-dropdown-link",
          onClick: e => e.preventDefault(),
          children: ["\u66F4\u591A ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(_ant_design_icons__WEBPACK_IMPORTED_MODULE_19__["DownOutlined"], {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 152,
            columnNumber: 20
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 148,
          columnNumber: 15
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 147,
        columnNumber: 13
      }, undefined);
    }
  }]
}];

var ManageMembers = props => {
  _s();

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_18__["useState"])(false),
      _useState2 = Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_8__["default"])(_useState, 2),
      visible = _useState2[0],
      setVisible = _useState2[1];

  var _useTable = Object(_hooks__WEBPACK_IMPORTED_MODULE_22__["useTable"])(getTableData),
      tableProps = _useTable.tableProps,
      handleSearch = _useTable.handleSearch;

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_18__["useState"])(false),
      _useState4 = Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_8__["default"])(_useState3, 2),
      loading = _useState4[0],
      setLoading = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_18__["useState"])(),
      _useState6 = Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_8__["default"])(_useState5, 2),
      key = _useState6[0],
      setKey = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_18__["useState"])(columnList.map(x => x.key)),
      _useState8 = Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_8__["default"])(_useState7, 2),
      colKeys = _useState8[0],
      setColKeys = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_18__["useState"])(columnList),
      _useState10 = Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_8__["default"])(_useState9, 2),
      columns = _useState10[0],
      setColumns = _useState10[1];

  var handlerExport = /*#__PURE__*/function () {
    var _ref2 = Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_13__["default"])( /*#__PURE__*/_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_15___default.a.mark(function _callee2() {
      return _Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_15___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              setLoading(true);
              _context2.next = 3;
              return Object(_api_customers__WEBPACK_IMPORTED_MODULE_17__["exportCustomerCSV"])();

            case 3:
              setLoading(false);

            case 4:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function handlerExport() {
      return _ref2.apply(this, arguments);
    };
  }();

  var showDrawer = () => {
    setVisible(true);
  };

  var onClose = () => {
    setVisible(false);
  };

  var handleColumnsSettingChange = keys => {
    columns.forEach(item => {
      if (!item.children || !item.title) return;
      item.children = item.children.map(x => Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_14__["default"])(Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_14__["default"])({}, x), {}, {
        isShow: !keys.includes(x.key)
      }));
    });
    setColumns(columns);
    setColKeys(keys);
  };

  var handleColumns = data => {
    var newData = lodash__WEBPACK_IMPORTED_MODULE_20___default.a.cloneDeep(data);

    newData.forEach((item, index) => {
      if (!item.children) return;
      item.children = item.children.filter(x => !x.isShow);

      if (!item.children.length) {
        delete newData[index];
      }
    });
    return newData;
  };

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])("div", {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(_components_Title__WEBPACK_IMPORTED_MODULE_16__["default"], {
      value: "MANAGE MEMBERS"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 213,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(antd_es_drawer__WEBPACK_IMPORTED_MODULE_7__["default"], {
      title: "Setting ColumnList",
      visible: visible,
      onClose: onClose,
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(_ColumnsSetting__WEBPACK_IMPORTED_MODULE_25__["default"], {
        onChange: handleColumnsSettingChange,
        data: columns,
        defaultData: colKeys.filter(Boolean)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 215,
        columnNumber: 9
      }, undefined)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 214,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(_LineChart__WEBPACK_IMPORTED_MODULE_23__["default"], {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 221,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(_BarChart__WEBPACK_IMPORTED_MODULE_24__["default"], {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 222,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])("div", {
      className: "wil_table",
      style: {
        marginTop: 50
      },
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])("div", {
        className: "wil_table_header",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])("div", {
          className: "wil_table_search",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(antd_es_input__WEBPACK_IMPORTED_MODULE_5__["default"], {
            value: key,
            onChange: e => {
              setKey(key);
              handleSearch(e.target.value);
            },
            prefix: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(_ant_design_icons__WEBPACK_IMPORTED_MODULE_19__["SearchOutlined"], {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 232,
              columnNumber: 23
            }, undefined)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 226,
            columnNumber: 13
          }, undefined)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 225,
          columnNumber: 11
        }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])("div", {
          className: "wil_table_action",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])("div", {
            className: "wil_table_action_item",
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(antd_es_button__WEBPACK_IMPORTED_MODULE_3__["default"], {
              type: "primary",
              loading: loading,
              onClick: handlerExport,
              icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(_ant_design_icons__WEBPACK_IMPORTED_MODULE_19__["DownloadOutlined"], {}, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 241,
                columnNumber: 23
              }, undefined),
              children: "Export CSV"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 237,
              columnNumber: 15
            }, undefined)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 236,
            columnNumber: 13
          }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])("div", {
            className: "wil_table_action_item",
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(antd_es_button__WEBPACK_IMPORTED_MODULE_3__["default"], {
              type: "link",
              loading: loading,
              onClick: showDrawer,
              icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(_ant_design_icons__WEBPACK_IMPORTED_MODULE_19__["SettingOutlined"], {}, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 251,
                columnNumber: 23
              }, undefined)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 247,
              columnNumber: 15
            }, undefined)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 246,
            columnNumber: 13
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 235,
          columnNumber: 11
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 224,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_26__["jsxDEV"])(antd_es_table__WEBPACK_IMPORTED_MODULE_1__["default"], Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_14__["default"])(Object(_Users_max_lv_Documents_code_wilson_admin_portal_v1_node_modules_umijs_babel_preset_umi_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_14__["default"])({
        columns: handleColumns(columns),
        style: {
          padding: 20
        },
        rowKey: "key"
      }, tableProps), {}, {
        scroll: {
          x: 'calc(700px + 50%)',
          y: 800
        },
        bordered: true
      }), void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 257,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 223,
      columnNumber: 7
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 212,
    columnNumber: 5
  }, undefined);
};

_s(ManageMembers, "YScHG5NTHQo5lmJ33Zb5mtWDd8U=", false, function () {
  return [_hooks__WEBPACK_IMPORTED_MODULE_22__["useTable"]];
});

_c = ManageMembers;
/* harmony default export */ __webpack_exports__["default"] = (ManageMembers);

var _c;

__webpack_require__.$Refresh$.register(_c, "ManageMembers");

var $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
var $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	var errorOverlay;
	if (true) {
		errorOverlay = false;
	}
	var testMode;
	if (typeof __react_refresh_test__ !== 'undefined') {
		testMode = __react_refresh_test__;
	}
	return __react_refresh_utils__.executeRuntime(
		exports,
		$ReactRefreshModuleId$,
		module.hot,
		errorOverlay,
		testMode
	);
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/umi/node_modules/@umijs/preset-built-in/bundled/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/umi/node_modules/@umijs/preset-built-in/bundled/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js")))

/***/ })

})
//# sourceMappingURL=umi.392963482642d1192c04.hot-update.js.map