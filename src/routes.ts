import { DashboardOutlined, UserOutlined } from '@ant-design/icons';

const routes = [
  {
    path: '/',
    component: '@/pages/Home',
    name: 'Dashboard',
    icon: DashboardOutlined,
    hideInMenu: true
  },
  {
    exact: true,
    path: '/members',
    component: '@/pages/Customers',
    name: 'Manage Members',
    icon: UserOutlined,
    hideInMenu: true
  },
];

export { routes };
