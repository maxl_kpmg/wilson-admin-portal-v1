import { defineConfig } from 'umi';
import { routes } from './src/routes';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  locale: {
    default: 'zh-HK',
    antd: true
  },
  dva: {
    immer: true,
    hmr: false,
  },
  routes: [
    { path: '/login', component: '@/pages/Login' },
    {
      path: '/',
      component: '@/layouts/',
      routes: routes,
      wrappers: ['@/wrappers/auth']
    },
  ],
  theme: {
    '@primary-color': '#1ABCBA'
  },
  fastRefresh: {},
  devServer: {
    proxy: {
      '/sit': {
        target: 'https://wil-sit-ysj8tt.gowilapp.com.hk',
        changeOrigin: true,
        pathRewrite: { '/sit': '' },
      },
      '/dev': {
        target: 'https://wil-dev-ysh8tt.gowilapp.com.hk',
        changeOrigin: true,
        pathRewrite: { '/dev': '' },
      }
    }
  }
});
